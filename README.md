# README #

Скрипт проверки заливки на сервер файлов методами [WAScript](http://docs.waspace.net/doku.php/ru/wascript).

### Установка ###
1. Скопируйте файлы в любую папку на локальный сервер или хостинг.

### Тестирование ###
Используйте адрес скрипта при работе с WAScript.

### Пример ###
Пусть index.html и upload.php находятся по адресу http://mysite.com/uploadtest/.
Тогда код теста метода [TImage.SetAsFile](http://docs.waspace.net/doku.php/ru/wascript/classes/timage/setasfile) может выглядеть следующим образом:
~~~~
//{$MODE ECMA}
TABS[0].load('tests://captcha.html');
var image = TABS[0].getElements('#recaptcha_challenge_image')[0].image();
var tab2 = new TTab();
tab2.load('http://mysite.com/uploadtest/index.html');
image.setAsFile('captcha_as_file.png');
tab2.getElements('input[name=filename]')[0].mouseClick();
tab2.getElements('input[type=submit]')[0].mouseClick();
~~~~